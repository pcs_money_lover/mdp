package com.stts.mynews.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.stts.mynews.BaseActivity;
import com.stts.mynews.R;
import com.stts.mynews.adapter.NewsAdapter;
import com.stts.mynews.connection.ApiClientRoot;
import com.stts.mynews.connection.Rest;
import com.stts.mynews.helper.Pesan;
import com.stts.mynews.model.News;
import com.stts.mynews.response.BeritaResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Query;

public class BeritaKategoriActivity extends BaseActivity {

    private NewsAdapter adapter;
    private List<News> list = new ArrayList<>();
    private RecyclerView recyclerView;
    private String id_kategori = "";
    private String judul = "";
    private TextView title;
    private LinearLayout dummynews;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_kategori);
        initComponent();
        setOnlickComponent();
        id_kategori = getIntent().getStringExtra("idkategori");
        judul = getIntent().getStringExtra("judul");
        title.setText(judul);
        initRecyclerView();
        initValue();
    }
    public void initValue(){

        Rest service = new ApiClientRoot().getClient().create(Rest.class);
        Call<BeritaResponse> call = service.getSearchBerita("1","100","desc","3",id_kategori,"");
        call.enqueue(new Callback<BeritaResponse>() {
            @Override
            public void onResponse(Call<BeritaResponse> call, retrofit2.Response<BeritaResponse> response) {
                if(response.body() != null){
                    if(response.body().getStatus()== 1){
                        for (int i=0;i<response.body().getData().size();i++){
                            list.add(new News(response.body().getData().get(i).getIdNews()
                                    ,response.body().getData().get(i).getTitle(),
                                    response.body().getData().get(i).getDeskripsi(),
                                    response.body().getData().get(i).getGalery(),
                                    response.body().getData().get(i).getDateTime(),
                                    response.body().getData().get(i).getVideo()));
                        }
                        adapter.notifyDataSetChanged();
                        dummynews.setVisibility(View.GONE);
                    }else{
                        Toast.makeText(getApplicationContext(),""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<BeritaResponse> call, Throwable t) {
                Pesan.debug("coba",t.getMessage()+" ");
            }
        });
    }
    public void initRecyclerView(){

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setFocusable(false);
        adapter = new NewsAdapter(BeritaKategoriActivity.this,getApplicationContext(),list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }
        });
        adapter.notifyDataSetChanged();
    }
    @Override
    public void setOnlickComponent() {
        super.setOnlickComponent();
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void initComponent() {
        super.initComponent();
        title = findViewById(R.id.title);
        dummynews = findViewById(R.id.dummynews);
        dummynews.setVisibility(View.VISIBLE);
    }
}
