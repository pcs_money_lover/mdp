package com.stts.mynews.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.stts.mynews.BaseActivity;
import com.stts.mynews.R;
import com.stts.mynews.connection.ApiClientRoot;
import com.stts.mynews.connection.Rest;
import com.stts.mynews.database.User;
import com.stts.mynews.helper.Pesan;
import com.stts.mynews.helper.ServiceRealm;
import com.stts.mynews.request.ChangeProfilRequest;
import com.stts.mynews.request.LoginRequest;
import com.stts.mynews.response.ChangeProfilResponse;
import com.stts.mynews.response.LoginResponse;
import com.stts.mynews.response.MyProfilResponse;

import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;

public class ChangeAccountActivity extends BaseActivity {
    private EditText name,email,phone;
    private Button simpan;
    private RealmResults<User> users;
    private ProgressDialog progressdialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_account);
        progressdialog = new ProgressDialog(ChangeAccountActivity.this);
        users= ServiceRealm.getrealm(getApplicationContext()).where(User.class).findAll();
        initComponent();
        setOnlickComponent();
        getProfil(users.get(0).getToken());
    }

    public void openDialog(){
        progressdialog.setCancelable(false);
        progressdialog.setMessage("Please Wait....");
        progressdialog.show();
        simpan(users.get(0).getToken());
    }
    @Override
    public void setOnlickComponent() {
        super.setOnlickComponent();
        simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });
    }

    public void getProfil(String token){

        users= ServiceRealm.getrealm(getApplicationContext()).where(User.class).findAll();

        Rest service = new ApiClientRoot().getClient().create(Rest.class);
        Call<MyProfilResponse> call = service.getProfil(token);
        call.enqueue(new Callback<MyProfilResponse>() {
            @Override
            public void onResponse(Call<MyProfilResponse> call, retrofit2.Response<MyProfilResponse> response) {
                if(response.body() != null){
                    if(response.body().getStatus()== 1){

                        name.setText(response.body().getData().getName());
                        email.setText(response.body().getData().getEmail());
                        phone.setText(response.body().getData().getPhone());
                    }else{
                        Toast.makeText(getApplicationContext(),""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<MyProfilResponse> call, Throwable t) {
                Pesan.debug("coba",t.getMessage()+" ");
            }
        });
    }

    public void simpan(String token){

        Rest service = new ApiClientRoot().getClient().create(Rest.class);
        Call<ChangeProfilResponse> call = service.postChangeProfil(token,new ChangeProfilRequest(name.getText().toString(),
                email.getText().toString(),phone.getText().toString()));
        call.enqueue(new Callback<ChangeProfilResponse>() {
            @Override
            public void onResponse(Call<ChangeProfilResponse> call, retrofit2.Response<ChangeProfilResponse> response) {
                if(response.body() != null){
                    if(response.body().getStatus()== 1){
                        progressdialog.dismiss();
                        Toast.makeText(getApplicationContext(),""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }else{
                        progressdialog.dismiss();
                        Toast.makeText(getApplicationContext(),""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ChangeProfilResponse> call, Throwable t) {
                Pesan.debug("coba",t.getMessage()+" ");
            }
        });

    }
    @Override
    public void initComponent() {
        super.initComponent();
        name = findViewById(R.id.name);
        email = findViewById(R.id.email);
        phone = findViewById(R.id.phone);
        simpan = findViewById(R.id.simpan);
    }
}
