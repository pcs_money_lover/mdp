package com.stts.mynews.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.stts.mynews.BaseActivity;
import com.stts.mynews.R;
import com.stts.mynews.connection.ApiClientRoot;
import com.stts.mynews.connection.Rest;
import com.stts.mynews.database.User;
import com.stts.mynews.helper.Pesan;
import com.stts.mynews.helper.ServiceRealm;
import com.stts.mynews.request.ChangePasswordRequest;
import com.stts.mynews.request.ChangeProfilRequest;
import com.stts.mynews.response.ChangePasswordResponse;
import com.stts.mynews.response.ChangeProfilResponse;

import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;

public class ChangePasswordActivity extends BaseActivity {
    private EditText cpassword,npassword,ncpassword;
    private Button simpan;
    private RealmResults<User> users;
    private ProgressDialog progressdialog;
    private ImageView see,see1,see2;
    private Boolean seepass = false;
    private Boolean seepass1 = false;
    private Boolean seepass2 = false;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        progressdialog = new ProgressDialog(ChangePasswordActivity.this);
        users= ServiceRealm.getrealm(getApplicationContext()).where(User.class).findAll();
        initComponent();
        setOnlickComponent();
    }

    public void openDialog(){
        progressdialog.setCancelable(false);
        progressdialog.setMessage("Please Wait....");
        progressdialog.show();
        simpan(users.get(0).getToken());
    }
    @Override
    public void setOnlickComponent() {
        super.setOnlickComponent();
        simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });
        see.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!seepass){
                    seepass = true;
                    cpassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                }else{
                    seepass = false;
                    cpassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

                }
            }
        });
        see1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!seepass1){
                    seepass1 = true;
                    npassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                }else{
                    seepass1 = false;
                    npassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

                }
            }
        });
        see2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!seepass2){
                    seepass2 = true;
                    ncpassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                }else{
                    seepass2 = false;
                    ncpassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

                }
            }
        });
    }


    public void simpan(String token){

        Rest service = new ApiClientRoot().getClient().create(Rest.class);
        Call<ChangePasswordResponse> call = service.postChangePassword(token,new ChangePasswordRequest(cpassword.getText().toString(),
                npassword.getText().toString(),ncpassword.getText().toString()));
        call.enqueue(new Callback<ChangePasswordResponse>() {
            @Override
            public void onResponse(Call<ChangePasswordResponse> call, retrofit2.Response<ChangePasswordResponse> response) {
                if(response.body() != null){
                    if(response.body().getStatus()== 1){
                        progressdialog.dismiss();
                        Toast.makeText(getApplicationContext(),""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }else{
                        progressdialog.dismiss();
                        Toast.makeText(getApplicationContext(),""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ChangePasswordResponse> call, Throwable t) {
                Pesan.debug("coba",t.getMessage()+" ");
            }
        });

    }
    @Override
    public void initComponent() {
        super.initComponent();
        cpassword = findViewById(R.id.cpassword);
        npassword = findViewById(R.id.npassword);
        ncpassword = findViewById(R.id.ncpassword);
        simpan = findViewById(R.id.simpan);
        see  = findViewById(R.id.see);
        see1  = findViewById(R.id.see1);
        see2  = findViewById(R.id.see2);
    }
}
