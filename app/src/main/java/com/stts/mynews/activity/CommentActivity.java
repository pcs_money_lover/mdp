package com.stts.mynews.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.stts.mynews.BaseActivity;
import com.stts.mynews.R;
import com.stts.mynews.adapter.CommentAdapter;
import com.stts.mynews.adapter.NotificationAdapter;
import com.stts.mynews.connection.ApiClientRoot;
import com.stts.mynews.connection.Rest;
import com.stts.mynews.database.User;
import com.stts.mynews.helper.Pesan;
import com.stts.mynews.helper.ServiceRealm;
import com.stts.mynews.model.Comment;
import com.stts.mynews.model.Notification;
import com.stts.mynews.request.CommentRequest;
import com.stts.mynews.response.CommentPostResponse;
import com.stts.mynews.response.CommentResponse;
import com.stts.mynews.response.MyProfilResponse;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;

public class CommentActivity extends BaseActivity {

    private CommentAdapter adapter;
    private EditText comment;
    private List<Comment> list = new ArrayList<>();
    private RecyclerView recyclerView;
    private RealmResults<User> users;
    private String id = "";
    private Boolean krmkomen = true;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        users= ServiceRealm.getrealm(getApplicationContext()).where(User.class).findAll();
        id = getIntent().getStringExtra("id");
        initRecyclerView();
        initValue();
        initComponent();
        setOnlickComponent();
    }
    public void getComment(){

        Rest service = new ApiClientRoot().getClient().create(Rest.class);
        Call<CommentPostResponse> call = service.getCommentPost("1","100",""+id,"asc");
        call.enqueue(new Callback<CommentPostResponse>() {
            @Override
            public void onResponse(Call<CommentPostResponse> call, retrofit2.Response<CommentPostResponse> response) {
                if(response.body() != null){
                    if(response.body().getStatus()== 1){
                        for(int i=0;i<response.body().getData().size();i++){

                            list.add(new Comment(""+response.body().getData().get(i).getIdNewsComment()
                                    ,""+response.body().getData().get(i).getIdNews(),
                                    ""+response.body().getData().get(i).getName(),
                                    ""+response.body().getData().get(i).getDeskripsi(),
                                    ""+response.body().getData().get(i).getDateTime()));
                        }
                    }else{
                        Toast.makeText(getApplicationContext(),""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<CommentPostResponse> call, Throwable t) {
                Pesan.debug("coba",t.getMessage()+" ");
            }
        });
    }
    public void initValue(){
        getComment();
        adapter.notifyDataSetChanged();
    }
    public void initRecyclerView(){

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setFocusable(false);
        adapter = new CommentAdapter(CommentActivity.this,getApplicationContext(),list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
    @Override
    public void setOnlickComponent() {
        super.setOnlickComponent();
        comment = findViewById(R.id.comment);

    }

    @Override
    public void initComponent() {
        super.initComponent();
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        findViewById(R.id.send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(krmkomen){
                    krmkomen = false;
                    sendComment();
                }
            }
        });
    }
    public void sendComment(){

        Rest service = new ApiClientRoot().getClient().create(Rest.class);
        Call<CommentResponse> call = service.postComment(users.get(0).getToken(),new CommentRequest(id,
                comment.getText().toString()));
        call.enqueue(new Callback<CommentResponse>() {
            @Override
            public void onResponse(Call<CommentResponse> call, retrofit2.Response<CommentResponse> response) {
                if(response.body() != null){
                    if(response.body().getStatus()== 1){
                        list.add(new Comment(""
                                ,"",
                                ""+response.body().getData().getNama(),
                                ""+comment.getText().toString(),
                                ""+response.body().getData().getDate_time()));
                        adapter.notifyDataSetChanged();
                        krmkomen = true;
                        comment.setText("");
                    }else{
                        krmkomen = true;
                        Toast.makeText(getApplicationContext(),""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<CommentResponse> call, Throwable t) {
                Pesan.debug("coba",t.getMessage()+" ");
            }
        });
    }

}
