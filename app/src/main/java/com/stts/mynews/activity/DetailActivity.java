package com.stts.mynews.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pierfrancescosoffritti.youtubeplayer.player.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayer;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerInitListener;
import com.pierfrancescosoffritti.youtubeplayer.player.YouTubePlayerView;
import com.squareup.picasso.Picasso;
import com.stts.mynews.BaseActivity;
import com.stts.mynews.R;
import com.stts.mynews.connection.ApiClientRoot;
import com.stts.mynews.connection.Rest;
import com.stts.mynews.database.BookmarkBerita;
import com.stts.mynews.database.User;
import com.stts.mynews.helper.Pesan;
import com.stts.mynews.helper.ServiceRealm;
import com.stts.mynews.request.FavoritRequest;
import com.stts.mynews.request.LoginRequest;
import com.stts.mynews.request.ReadCloseRequest;
import com.stts.mynews.request.ReadOpenRequest;
import com.stts.mynews.request.SaveLaterRequest;
import com.stts.mynews.response.FavoritResponse;
import com.stts.mynews.response.LikedResponse;
import com.stts.mynews.response.LoginResponse;
import com.stts.mynews.response.ReadCloseResponse;
import com.stts.mynews.response.ReadOpenResponse;
import com.stts.mynews.response.SaveLaterResponse;

import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;

public class DetailActivity extends BaseActivity {
    private TextView title,desc,tbookmark,tfavorit,date;
    private ImageView img,icbookmark,icfavorit;
    private String id ="";
    private String id_open = "";
    private RealmResults<User> users;
    private RealmResults<BookmarkBerita> bookmarkBeritas;

    YouTubePlayerView youtube_player_view;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        youtube_player_view = findViewById(R.id.youtube_player_view);

        users= ServiceRealm.getrealm(getApplicationContext()).where(User.class).findAll();
        bookmarkBeritas= ServiceRealm.getrealm(getApplicationContext()).where(BookmarkBerita.class).findAll();
        initComponent();
        setOnlickComponent();
        initValue();
        open();
    }

    public void open(){

        Rest service = new ApiClientRoot().getClient().create(Rest.class);
        Call<ReadOpenResponse> call = service.postReadOpen(users.get(0).getToken(),new ReadOpenRequest(id));
        call.enqueue(new Callback<ReadOpenResponse>() {
            @Override
            public void onResponse(Call<ReadOpenResponse> call, retrofit2.Response<ReadOpenResponse> response) {
                if(response.body() != null){
                    if(response.body().getStatus()== 1){
                        id_open = response.body().getId_oldest_reader();
                    }else{
                    }
                }
            }

            @Override
            public void onFailure(Call<ReadOpenResponse> call, Throwable t) {
                Pesan.debug("coba",t.getMessage()+" ");
            }
        });

    }
    public void close(){

        Rest service = new ApiClientRoot().getClient().create(Rest.class);
        Call<ReadCloseResponse> call = service.postReadClose(users.get(0).getToken(),new ReadCloseRequest(id_open));
        call.enqueue(new Callback<ReadCloseResponse>() {
            @Override
            public void onResponse(Call<ReadCloseResponse> call, retrofit2.Response<ReadCloseResponse> response) {
                if(response.body() != null){
                    if(response.body().getStatus()== 1){
                        Log.d("tutup","tutup");

                        finish();
                    }else{
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<ReadCloseResponse> call, Throwable t) {
                Pesan.debug("coba",t.getMessage()+" ");
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
        close();
    }

    public void initValue(){
        id = getIntent().getStringExtra("id");
        title.setText(""+getIntent().getStringExtra("title"));
        desc.setText(""+getIntent().getStringExtra("desc"));
        Picasso.get().load(getIntent().getStringExtra("img")).into(img);
        bookmarkBeritas= ServiceRealm.getrealm(getApplicationContext()).where(BookmarkBerita.class).findAll();
        for(int i=0;i<bookmarkBeritas.size();i++){
            if(bookmarkBeritas.get(i).getIdBerita() != null && id != null){
                if(id.equalsIgnoreCase(bookmarkBeritas.get(i).getIdBerita())){
                    setBookmarked();
                }
            }
        }
        checkLiked();
        final String video = getIntent().getStringExtra("video");
        //Toast.makeText(getApplicationContext(),""+video,Toast.LENGTH_SHORT).show();
        Intent intent = getIntent();

        if(intent.hasExtra("video")){
            if(!video.equalsIgnoreCase("")){
                youtube_player_view.initialize(new YouTubePlayerInitListener() {
                    @Override
                    public void onInitSuccess(final YouTubePlayer youTubePlayer) {
                        youTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                            @Override
                            public void onReady() {
                                String[] idyt = video.split("=");
                                youTubePlayer.loadVideo(idyt[1], 0);
                                youTubePlayer.pause();
                            }
                        });
                    }
                },true);
            }else{
                youtube_player_view.setVisibility(View.GONE);
            }
        }else{
            youtube_player_view.setVisibility(View.GONE);
        }

    }

    public void setBookmarked(){
        icbookmark.setImageResource(R.drawable.ic_bookmark_black_24dp);
        tbookmark.setText("SAVED");
        tbookmark.setTextColor(Color.argb(255,214,214,214));
    }
    public void setFavorit(){
        icfavorit.setImageResource(R.drawable.ic_favorite_black_24dp);
        tfavorit.setText("LIKED");
        tfavorit.setTextColor(Color.argb(255,100,100,100));
    }
    public void saveArticle(){
        new ServiceRealm().getrealm(getApplicationContext()).beginTransaction();
        BookmarkBerita db = new ServiceRealm().getrealm(getApplicationContext()).createObject(BookmarkBerita.class);
        db.setDateTime(""+getIntent().getStringExtra("date"));
        db.setIdBerita(""+getIntent().getStringExtra("id"));
        db.setDeskripsi(""+getIntent().getStringExtra("desc"));
        db.setImg(""+getIntent().getStringExtra("img"));
        db.setJudul(""+getIntent().getStringExtra("title"));
        new ServiceRealm().getrealm(getApplicationContext()).commitTransaction();
        Toast.makeText(getApplicationContext(),"Berhasil dibookmark.",Toast.LENGTH_SHORT).show();
        setBookmarked();
    }

    public void favorit(){

        Rest service = new ApiClientRoot().getClient().create(Rest.class);
        Call<FavoritResponse> call = service.postFavorit(users.get(0).getToken(),new FavoritRequest(id));
        call.enqueue(new Callback<FavoritResponse>() {
            @Override
            public void onResponse(Call<FavoritResponse> call, retrofit2.Response<FavoritResponse> response) {
                if(response.body() != null){
                    if(response.body().getStatus()== 1){
                        Toast.makeText(getApplicationContext(),""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        setFavorit();
                    }else{
                        Toast.makeText(getApplicationContext(),""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<FavoritResponse> call, Throwable t) {
                Pesan.debug("coba",t.getMessage()+" ");
            }
        });

    }
    public void checkLiked(){

        Rest service = new ApiClientRoot().getClient().create(Rest.class);
        Call<LikedResponse> call = service.getLiked(users.get(0).getToken(),id);
        call.enqueue(new Callback<LikedResponse>() {
            @Override
            public void onResponse(Call<LikedResponse> call, retrofit2.Response<LikedResponse> response) {
                if(response.body() != null){
                    if(response.body().getLiked().equalsIgnoreCase("1")){
                        setFavorit();
                    }
                }
            }

            @Override
            public void onFailure(Call<LikedResponse> call, Throwable t) {
                Pesan.debug("coba",t.getMessage()+" ");
            }
        });

    }

    public void saveLater(){

        Rest service = new ApiClientRoot().getClient().create(Rest.class);
        Call<SaveLaterResponse> call = service.postSaveLater(users.get(0).getToken(),new SaveLaterRequest(id));
        call.enqueue(new Callback<SaveLaterResponse>() {
            @Override
            public void onResponse(Call<SaveLaterResponse> call, retrofit2.Response<SaveLaterResponse> response) {
                if(response.body() != null){
                    Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SaveLaterResponse> call, Throwable t) {
                Pesan.debug("coba",t.getMessage()+" ");
            }
        });

    }
    @Override
    public void setOnlickComponent() {
        super.setOnlickComponent();
        findViewById(R.id.save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tbookmark.getText().toString().equalsIgnoreCase("BOOKMARKED")){
                    Toast.makeText(getApplicationContext(),"Berita sudah anda bookmark",Toast.LENGTH_SHORT).show();
                }else {
                    saveArticle();
                }
            }
        });
        findViewById(R.id.later).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveLater();
            }
        });
        findViewById(R.id.favorit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tfavorit.getText().toString().equalsIgnoreCase("LIKED")){
                    Toast.makeText(getApplicationContext(),"Berita sudah anda sukai",Toast.LENGTH_SHORT).show();
                }else {
                    favorit();
                }
            }
        });
        findViewById(R.id.comment).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),CommentActivity.class);
                i.putExtra("id",id);
                startActivity(i);
            }
        });
        findViewById(R.id.share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent txtIntent = new Intent(android.content.Intent.ACTION_SEND);
                txtIntent .setType("text/plain");
                txtIntent .putExtra(android.content.Intent.EXTRA_SUBJECT, getIntent().getStringExtra("title"));
                txtIntent .putExtra(android.content.Intent.EXTRA_TEXT, getIntent().getStringExtra("desc"));
                startActivity(Intent.createChooser(txtIntent ,"Share"));
            }
        });
    }

    @Override
    public void initComponent() {
        super.initComponent();
        title = findViewById(R.id.title);
        desc = findViewById(R.id.desc);
        img = findViewById(R.id.img);
        tbookmark= findViewById(R.id.tbookmark);
        tfavorit = findViewById(R.id.tfavorit);
        icbookmark = findViewById(R.id.icbookmark);
        icfavorit = findViewById(R.id.icfavorit);
        date = findViewById(R.id.date);
        date.setText(getIntent().getStringExtra("date"));

    }
}
