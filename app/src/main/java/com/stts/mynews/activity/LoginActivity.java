package com.stts.mynews.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.stts.mynews.BaseActivity;
import com.stts.mynews.R;
import com.stts.mynews.connection.ApiClientRoot;
import com.stts.mynews.connection.Rest;
import com.stts.mynews.database.User;
import com.stts.mynews.helper.Pesan;
import com.stts.mynews.helper.ServiceRealm;
import com.stts.mynews.request.LoginRequest;
import com.stts.mynews.request.RegisterRequest;
import com.stts.mynews.response.LoginResponse;
import com.stts.mynews.response.MyProfilResponse;
import com.stts.mynews.response.RegisterResponse;

import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;

public class LoginActivity extends BaseActivity {
    private EditText email,password;
    private Boolean seepass = false;
    private ProgressDialog progressdialog;
    private RealmResults<User> users;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        progressdialog = new ProgressDialog(LoginActivity.this);
        initComponent();
        setOnlickComponent();
    }

    public void openDialog(){
        progressdialog.setCancelable(false);
        progressdialog.setMessage("Please Wait....");
        progressdialog.show();
        login();
    }

    public void getProfil(String token){

        users= ServiceRealm.getrealm(getApplicationContext()).where(User.class).findAll();

        Rest service = new ApiClientRoot().getClient().create(Rest.class);
        Call<MyProfilResponse> call = service.getProfil(token);
        call.enqueue(new Callback<MyProfilResponse>() {
            @Override
            public void onResponse(Call<MyProfilResponse> call, retrofit2.Response<MyProfilResponse> response) {
                if(response.body() != null){
                    if(response.body().getStatus()== 1){
                        users= ServiceRealm.getrealm(getApplicationContext()).where(User.class).findAll();
                        new ServiceRealm().getrealm(getApplicationContext()).beginTransaction();
                        User db = new ServiceRealm().getrealm(getApplicationContext()).createObject(User.class);
                        db.setUsername(response.body().getData().getUsername());
                        db.setToken(response.body().getData().getToken());
                        db.setEmail(response.body().getData().getEmail());
                        new ServiceRealm().getrealm(getApplicationContext()).commitTransaction();

                        progressdialog.dismiss();
                        Toast.makeText(getApplicationContext(),"Login Berhasil", Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(getApplicationContext(),MainActivity.class);
                        startActivity(i);
                        finish();
                    }else{
                        Toast.makeText(getApplicationContext(),""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<MyProfilResponse> call, Throwable t) {
                Pesan.debug("coba",t.getMessage()+" ");
            }
        });
    }
    public void login(){

        Rest service = new ApiClientRoot().getClient().create(Rest.class);
        Call<LoginResponse> call = service.postLogin(new LoginRequest(email.getText().toString(),password.getText().toString()));
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, retrofit2.Response<LoginResponse> response) {
                if(response.body() != null){
                    if(response.body().getStatus()== 1){
                        getProfil(response.body().getToken());
                    }else{
                        progressdialog.dismiss();
                        Toast.makeText(getApplicationContext(),""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Pesan.debug("coba",t.getMessage()+" ");
            }
        });

    }
    @Override
    public void setOnlickComponent() {
        super.setOnlickComponent();
        findViewById(R.id.login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               openDialog();
            }
        });
        findViewById(R.id.signup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),RegisterActivity.class);
                startActivity(i);

            }
        });
        findViewById(R.id.see).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!seepass){
                    seepass = true;
                    password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                }else{
                    seepass = false;
                    password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

                }
            }
        });
    }

    @Override
    public void initComponent() {
        super.initComponent();
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
    }
}
