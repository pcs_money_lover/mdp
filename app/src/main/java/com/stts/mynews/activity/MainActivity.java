package com.stts.mynews.activity;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.SectionDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.stts.mynews.BaseActivity;
import android.*;
import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import com.stts.mynews.R;
import com.stts.mynews.connection.ApiClientRoot;
import com.stts.mynews.connection.Rest;
import com.stts.mynews.database.FCM;
import com.stts.mynews.database.User;
import com.stts.mynews.fragment.BookmarkFragment;
import com.stts.mynews.fragment.MenuFragment;
import com.stts.mynews.fragment.NotificationFragment;
import com.stts.mynews.helper.Pesan;
import com.stts.mynews.helper.ServiceRealm;
import com.stts.mynews.request.ChangeFCMRequest;
import com.stts.mynews.request.LoginRequest;
import com.stts.mynews.response.BeritaKategoriResponse;
import com.stts.mynews.response.ChangeFCMResponse;
import com.stts.mynews.response.LoginResponse;
import com.stts.mynews.response.MyProfilResponse;

public class MainActivity extends AppCompatActivity   {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private RealmResults<User> users;
    private ArrayList<String> arr = new ArrayList<>();
    private ArrayList<String> arr_id = new ArrayList<>();
    private RealmResults<FCM> fcms;
    private Drawer result;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fcms= ServiceRealm.getrealm(getApplicationContext()).where(FCM.class).findAll();
        users= ServiceRealm.getrealm(getApplicationContext()).where(User.class).findAll();
        initToolbar();
        initTab();
        initDrawer();
        addMenu();
        sendRegistrationToServer(fcms.get(0).getToken());
    }
    public void sendRegistrationToServer(String token){
        if(fcms.size() > 0) {
            Rest service = new ApiClientRoot().getClient().create(Rest.class);
            Call<ChangeFCMResponse> call = service.postFCM(users.get(0).getToken(), new ChangeFCMRequest(token));
            call.enqueue(new Callback<ChangeFCMResponse>() {
                @Override
                public void onResponse(Call<ChangeFCMResponse> call, retrofit2.Response<ChangeFCMResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() == 1) {
                            Log.d("berhasil", "berhasil");
                        }
                    }
                }

                @Override
                public void onFailure(Call<ChangeFCMResponse> call, Throwable t) {
                }
            });
        }
    }
    public void addMenu(){

        Rest service = new ApiClientRoot().getClient().create(Rest.class);
        Call<BeritaKategoriResponse> call = service.getBeritaKategori();
        call.enqueue(new Callback<BeritaKategoriResponse>() {
            @Override
            public void onResponse(Call<BeritaKategoriResponse> call, retrofit2.Response<BeritaKategoriResponse> response) {
                if(response.body() != null){
                    if(response.body().getStatus()== 1){
                        for(int i=0;i<response.body().getData().size();i++){

                            arr.add(response.body().getData().get(i).getName());
                            arr_id.add(response.body().getData().get(i).getIdNewsCategory());
                        }
                        initDrawer();
                    }else{
                        Toast.makeText(getApplicationContext(),""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<BeritaKategoriResponse> call, Throwable t) {
                Pesan.debug("coba",t.getMessage()+" ");
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        result.setSelectionAtPosition(1);
    }

    public void initDrawer(){

        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.header)
                .addProfiles(
                        new ProfileDrawerItem().withName(users.get(0).getUsername()).withEmail(users.get(0).getEmail()).withIcon(getResources().getDrawable(R.drawable.profile2))
                )
                .withOnAccountHeaderListener(new AccountHeader.OnAccountHeaderListener() {
                    @Override
                    public boolean onProfileChanged(View view, IProfile profile, boolean currentProfile) {
                        return false;
                    }
                })
                .build();
        SecondaryDrawerItem [] as= new SecondaryDrawerItem[3];
        as[0] =  new SecondaryDrawerItem().withName("Ubah Akun").withTextColor(Color.GRAY);
        as[1] =  new SecondaryDrawerItem().withName("Ubah Password").withTextColor(Color.GRAY);
        as[2] =  new SecondaryDrawerItem().withName("Keluar").withTextColor(Color.GRAY);

        SecondaryDrawerItem [] as1= new SecondaryDrawerItem[arr.size()];
        for(int i=0;i<arr.size();i++){
            as1[i] = new SecondaryDrawerItem().withName(arr.get(i)).withTextColor(Color.GRAY);
        }


        result = new DrawerBuilder()
                .withActivity(this)
                .withAccountHeader(headerResult)
                .withToolbar(toolbar)
                .addDrawerItems(
                        new SecondaryDrawerItem().withName("Beranda").withTextColor(Color.GRAY),
                        new SecondaryDrawerItem().withName("Berita Favorit").withTextColor(Color.GRAY),
                        new SecondaryDrawerItem().withName("Ranking Pembaca").withTextColor(Color.GRAY),
                        new SecondaryDrawerItem().withName("Berita Populer").withTextColor(Color.GRAY),
                        new SecondaryDrawerItem().withName("Baca Nanti").withTextColor(Color.GRAY),
                        new SectionDrawerItem().withName("Semua Kategori").withTextColor(Color.BLACK)


                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        return true;
                    }
                })
                .addDrawerItems(as1)
                .addDrawerItems(
                        new SectionDrawerItem().withName("Menu Lainnya").withTextColor(Color.BLACK))
                .addDrawerItems(as)
                .build();
        result.setOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
            @Override
            public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                Log.d("posisi",""+position+" "+result.getDrawerItems().size());
                if(position == result.getDrawerItems().size()){
                    new ServiceRealm().getrealm(getApplicationContext()).beginTransaction();
                    RealmResults<User> users= ServiceRealm.getrealm(getApplicationContext()).where(User.class).findAll();
                    users.get(0).deleteFromRealm();
                    new ServiceRealm().getrealm(getApplicationContext()).commitTransaction();

                    Intent i = new Intent(getApplicationContext(),LoginActivity.class);
                    startActivity(i);
                    finish();
                }else if(position == result.getDrawerItems().size() -1){
                    Intent i = new Intent(getApplicationContext(),ChangePasswordActivity.class);
                    startActivity(i);
                }else if(position == result.getDrawerItems().size() -2){
                    Intent i = new Intent(getApplicationContext(),ChangeAccountActivity.class);
                    startActivity(i);
                }else if(position == 2){
                    Intent i = new Intent(getApplicationContext(),BeritaFavoritActivity.class);
                    startActivity(i);
                }else if(position == 3){
                    Intent i = new Intent(getApplicationContext(),RankingActivity.class);
                    startActivity(i);
                }else if(position == 4){
                    Intent i = new Intent(getApplicationContext(),BeritaPopulerActivity.class);
                    startActivity(i);
                }else if(position == 5){
                    Intent i = new Intent(getApplicationContext(),BeritaLaterActivity.class);
                    startActivity(i);
                }else if(position == 1){
                }else{
                    String idberita = arr_id.get(position-7);
                    String judul = arr.get(position-7);
                    Intent i = new Intent(getApplicationContext(),BeritaKategoriActivity.class);
                    i.putExtra("idkategori",""+idberita);
                    i.putExtra("judul",""+judul);
                    startActivity(i);
                }
                return false;
            }
        });
    }

    public void initTab(){
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorHeight(0);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                tabLayout.setupWithViewPager(viewPager);
                tabLayout.setSelectedTabIndicatorHeight(0);
                setupTabIcons(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        setupTabIcons(0);
    }
    public void initToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
    }
    public void permission(){
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {


            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.CALL_PHONE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                                ,android.Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},
                        1);

            }
        }
    }
    private void setupTabIcons(int position) {
        if(position == 0) {
            TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
            tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_home_active, 0, 0);
            tabLayout.getTabAt(0).setCustomView(tabOne);

            TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
            tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.baseline_bookmark_black_24active, 0, 0);
            tabLayout.getTabAt(1).setCustomView(tabTwo);

            TextView tabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
            tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_histori, 0, 0);
            tabLayout.getTabAt(2).setCustomView(tabThree);
        }else if(position == 1) {
            TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
            tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_home, 0, 0);
            tabLayout.getTabAt(0).setCustomView(tabOne);

            TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
            tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.baseline_bookmark_black_24, 0, 0);
            tabLayout.getTabAt(1).setCustomView(tabTwo);

            TextView tabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
            tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_histori, 0, 0);
            tabLayout.getTabAt(2).setCustomView(tabThree);
        }else if(position == 2) {
            TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
            tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_home, 0, 0);
            tabLayout.getTabAt(0).setCustomView(tabOne);

            TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
            tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.baseline_bookmark_black_24active, 0, 0);
            tabLayout.getTabAt(1).setCustomView(tabTwo);

            TextView tabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
            tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_update_active, 0, 0);
            tabLayout.getTabAt(2).setCustomView(tabThree);

        }

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new MenuFragment(), null);
        adapter.addFragment(new BookmarkFragment(),null);
        adapter.addFragment(new NotificationFragment(), null);

        viewPager.setAdapter(adapter);
    }


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {

            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            search();
        }

        return super.onOptionsItemSelected(item);
    }


    public void search() {

        final Dialog dialog;
        dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_search);
        final Spinner ssid = dialog.findViewById(R.id.ssdi);
        ArrayList<String> filter = new ArrayList<>();
        filter.add("asc");
        filter.add("desc");
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,filter);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ssid.setAdapter(adapter);
        final EditText password = dialog.findViewById(R.id.password);
        dialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),SearchActivity.class);
                i.putExtra("filter",""+ssid.getSelectedItem());
                i.putExtra("keyword",""+password.getText().toString());
                startActivity(i);
            }
        });
        dialog.show();
    }

}