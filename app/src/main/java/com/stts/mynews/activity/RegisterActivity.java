package com.stts.mynews.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.stts.mynews.BaseActivity;
import com.stts.mynews.R;
import com.stts.mynews.connection.ApiClientRoot;
import com.stts.mynews.connection.Rest;
import com.stts.mynews.helper.Pesan;
import com.stts.mynews.request.RegisterRequest;
import com.stts.mynews.response.RegisterResponse;

import retrofit2.Call;
import retrofit2.Callback;

public class RegisterActivity extends BaseActivity {
    private EditText email,name,phone,password;
    private Boolean seepass= false;
    private ProgressDialog progressdialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        progressdialog = new ProgressDialog(RegisterActivity.this);
        initComponent();
        setOnlickComponent();
    }
    public void openDialog(){
        progressdialog.setCancelable(false);
        progressdialog.setMessage("Please Wait....");
        progressdialog.show();
        register();
    }
    public void setEmpty(){
        email.setText("");
        name.setText("");
        phone.setText("");
        password.setText("");
    }

    public void register(){

        Rest service = new ApiClientRoot().getClient().create(Rest.class);
        Call<RegisterResponse> call = service.postRegister(new RegisterRequest(email.getText().toString(),
                password.getText().toString(),name.getText().toString(),email.getText().toString(),phone.getText().toString()));
        call.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, retrofit2.Response<RegisterResponse> response) {
                if(response.body() != null){
                    if(response.body().getStatus()== 1){

                        progressdialog.dismiss();
                        Toast.makeText(getApplicationContext(),""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        setEmpty();
                        finish();
                    }else{
                        progressdialog.dismiss();
                        Toast.makeText(getApplicationContext(),""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                Pesan.debug("coba",t.getMessage()+" ");
            }
        });

    }
    @Override
    public void setOnlickComponent() {
        super.setOnlickComponent();
        findViewById(R.id.login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),LoginActivity.class);
                startActivity(i);
            }
        });
        findViewById(R.id.signup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();

            }
        });
        findViewById(R.id.see).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!seepass){
                    seepass = true;
                    password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                }else{
                    seepass = false;
                    password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

                }
            }
        });
    }

    @Override
    public void initComponent() {
        super.initComponent();
        email = findViewById(R.id.email);
        name  = findViewById(R.id.name);
        phone  = findViewById(R.id.phone);
        password = findViewById(R.id.password);
    }
}
