package com.stts.mynews.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.stts.mynews.BaseActivity;
import com.stts.mynews.R;
import com.stts.mynews.database.User;
import com.stts.mynews.helper.ServiceRealm;

import io.realm.RealmResults;

public class SplashScreen extends BaseActivity {

    private RealmResults<User> users;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    Thread.sleep(3000);
                }catch (Exception e){

                }
                users= ServiceRealm.getrealm(getApplicationContext()).where(User.class).findAll();
                if(users.size()<=0) {
                    Intent i = new Intent(getApplicationContext(),LoginActivity.class);
                    startActivity(i);
                    finish();
                }else{
                    Intent i = new Intent(getApplicationContext(),MainActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        });
        th.start();
    }
}
