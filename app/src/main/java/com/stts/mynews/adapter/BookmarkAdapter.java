package com.stts.mynews.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.stts.mynews.R;
import com.stts.mynews.activity.DetailActivity;
import com.stts.mynews.model.Bookmark;
import com.stts.mynews.model.News;

import java.util.List;

/**
 * Created by Ravi Tamada on 18/05/16.
 */
public class BookmarkAdapter extends RecyclerView.Adapter<BookmarkAdapter.MyViewHolder> {

    private Context mContext;
    private Activity activity;
    private List<Bookmark> albumList;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout container_recycle;
        TextView desc,title;
        public MyViewHolder(View view) {
            super(view);
            desc = view.findViewById(R.id.desc);
            title = view.findViewById(R.id.title);
            container_recycle = view.findViewById(R.id.container_recycle);
        }
    }


    public BookmarkAdapter(Activity activity, Context mContext, List<Bookmark> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_bookmark, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Bookmark album = albumList.get(position);
        holder.title.setText(album.getJudul());
        holder.desc.setText(album.getDeskripsi());
        holder.container_recycle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, DetailActivity.class);
                i.putExtra("id",""+album.getIdBerita());
                i.putExtra("date",""+album.getDateTime());
                i.putExtra("title",""+album.getJudul());
                i.putExtra("desc",""+album.getDeskripsi());
                i.putExtra("img",""+album.getImg());
                activity.startActivity(i);
            }
        });
    }


    @Override
    public int getItemCount() {
        return albumList.size();
    }
}
