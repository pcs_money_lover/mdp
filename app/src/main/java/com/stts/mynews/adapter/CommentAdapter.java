package com.stts.mynews.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.stts.mynews.R;
import com.stts.mynews.activity.DetailActivity;
import com.stts.mynews.model.Comment;
import com.stts.mynews.model.Notification;

import java.util.List;

/**
 * Created by Ravi Tamada on 18/05/16.
 */
public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.MyViewHolder> {

    private Context mContext;
    private Activity activity;
    private List<Comment> albumList;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name,date,text;
        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            date = view.findViewById(R.id.date);
            text = view.findViewById(R.id.text);
        }
    }


    public CommentAdapter(Activity activity, Context mContext, List<Comment> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_comment, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Comment album = albumList.get(position);
        holder.name.setText(album.getIdUser());
        holder.text.setText(album.getDeskripsi());
        holder.date.setText(album.getDateTime());
    }


    @Override
    public int getItemCount() {
        return albumList.size();
    }
}
