package com.stts.mynews.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.stts.mynews.activity.DetailActivity;
import com.stts.mynews.model.News;
import com.stts.mynews.R;

import java.util.List;

/**
 * Created by Ravi Tamada on 18/05/16.
 */
public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.MyViewHolder> {

    private Context mContext;
    private Activity activity;
    private List<News> albumList;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout container_recycle;
        TextView desc,title,date;
        ImageView img;
        public MyViewHolder(View view) {
            super(view);
            container_recycle = view.findViewById(R.id.container_recycle);
            desc = view.findViewById(R.id.desc);
            title = view.findViewById(R.id.title);
            date = view.findViewById(R.id.date);
            img = view.findViewById(R.id.img);
        }
    }


    public NewsAdapter(Activity activity, Context mContext, List<News> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_news, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final News album = albumList.get(position);
        holder.title.setText(album.getJudul());
        holder.desc.setText(album.getDeskripsi());
        holder.date.setText(album.getDateTime());
        Picasso.get().load(album.getImg()).into(holder.img);
        holder.container_recycle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, DetailActivity.class);
                i.putExtra("id",""+album.getIdBerita());
                i.putExtra("date",""+album.getDateTime());
                i.putExtra("title",""+album.getJudul());
                i.putExtra("desc",""+album.getDeskripsi());
                i.putExtra("video",""+album.getVideo());
                i.putExtra("img",""+album.getImg());
                activity.startActivity(i);
            }
        });

    }


    @Override
    public int getItemCount() {
        return albumList.size();
    }
}
