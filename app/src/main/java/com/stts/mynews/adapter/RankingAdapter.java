package com.stts.mynews.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.stts.mynews.R;
import com.stts.mynews.activity.DetailActivity;
import com.stts.mynews.helper.DateTime;
import com.stts.mynews.model.Notification;
import com.stts.mynews.model.Ranking;

import java.util.Date;
import java.util.List;

/**
 * Created by Ravi Tamada on 18/05/16.
 */
public class RankingAdapter extends RecyclerView.Adapter<RankingAdapter.MyViewHolder> {

    private Context mContext;
    private Activity activity;
    private List<Ranking> albumList;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout container_recycle;
        TextView total,title,time,nomor;
        public MyViewHolder(View view) {
            super(view);
            total = view.findViewById(R.id.total);
            time = view.findViewById(R.id.time);
            nomor = view.findViewById(R.id.nomor);
            title = view.findViewById(R.id.title);
            container_recycle = view.findViewById(R.id.container_recycle);
        }
    }


    public RankingAdapter(Activity activity, Context mContext, List<Ranking> albumList) {
        this.mContext = mContext;
        this.albumList = albumList;
        this.activity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycle_ranking, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Ranking album = albumList.get(position);
        holder.title.setText(album.getName());
        int nmr = position+1;
        holder.nomor.setText(""+nmr);
        holder.time.setText(DateTime.getMinute(Integer.parseInt(album.getLama())));
        holder.total.setText(album.getJumlah_news()+" Berita");

    }


    @Override
    public int getItemCount() {
        return albumList.size();
    }
}
