package com.stts.mynews.connection;

import com.stts.mynews.request.ChangeFCMRequest;
import com.stts.mynews.request.ChangePasswordRequest;
import com.stts.mynews.request.ChangeProfilRequest;
import com.stts.mynews.request.CommentRequest;
import com.stts.mynews.request.FavoritRequest;
import com.stts.mynews.request.LikeRequest;
import com.stts.mynews.request.LoginRequest;
import com.stts.mynews.request.ReadCloseRequest;
import com.stts.mynews.request.ReadOpenRequest;
import com.stts.mynews.request.RegisterRequest;
import com.stts.mynews.request.SaveLaterRequest;
import com.stts.mynews.response.BeritaKategoriResponse;
import com.stts.mynews.response.BeritaResponse;
import com.stts.mynews.response.ChangeFCMResponse;
import com.stts.mynews.response.ChangePasswordResponse;
import com.stts.mynews.response.ChangeProfilResponse;
import com.stts.mynews.response.CommentPostResponse;
import com.stts.mynews.response.CommentResponse;
import com.stts.mynews.response.FavoritResponse;
import com.stts.mynews.response.LikeResponse;
import com.stts.mynews.response.LikedResponse;
import com.stts.mynews.response.LoginResponse;
import com.stts.mynews.response.MyProfilResponse;
import com.stts.mynews.response.RankingResponse;
import com.stts.mynews.response.ReadCloseResponse;
import com.stts.mynews.response.ReadOpenResponse;
import com.stts.mynews.response.RegisterResponse;
import com.stts.mynews.response.SaveLaterResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by user on 3/11/16.
 */
public interface Rest {

    //berita?page=1&total=15&id_news&filter=desc
    @GET("berita")
    Call<BeritaResponse> getBerita(@Query("page") String page,
                                   @Query("total") String total,
                                   @Query("id_news") String id_news,
                                   @Query("filter") String filter);

    //mynews.namagz.com/api/berita_populer?page=1&total=20&filter=desc
    @GET("berita_populer")
    Call<BeritaResponse> getBeritaPopuler(@Query("page") String page,
                                   @Query("total") String total,
                                   @Query("id_news") String id_news,
                                   @Query("filter") String filter);
    @GET("berita_pinned")
    Call<BeritaResponse> getBeritaPinned();

    @GET("Berita_read_later")
    Call<BeritaResponse> getBeritaLater(@Header("token") String token);
    //mynews.namagz.com/api/Berita_search?page=1&total=5&filter=asc&type=2&id_news_category=6&key=Blockchain

    @GET("Berita_search")
    Call<BeritaResponse> getBeritaSearch(@Query("page") String page,
                                         @Query("total") String total,
                                         @Query("id_news_category") String id_news_category,
                                         @Query("filter") String filter,
                                         @Query("type") String type,
                                         @Query("key") String key);

    @GET("Berita_favorit")
    Call<BeritaResponse> getBeritaFavorit(@Header("token") String token);

    @GET("Berita_comment")
    Call<CommentPostResponse> getCommentPost(@Query("page") String page,
                                             @Query("total") String total,
                                             @Query("id_news") String id_news,
                                             @Query("filter") String filter);
    @GET("user")
    Call<MyProfilResponse> getProfil(@Header("token") String token);

    //Check_Favorit?id_news=52
    @GET("Check_Favorit")
    Call<LikedResponse> getLiked(@Header("token") String token, @Query("id_news") String id_news);


    @GET("berita_kategori")
    Call<BeritaKategoriResponse> getBeritaKategori();


    //mynews.namagz.com/api/Berita_search?page=1&total=5&filter=asc&type=3&id_news_category=6&key=bola
    @GET("Berita_search")
    Call<BeritaResponse> getSearchBerita(@Query("page") String page,
                                   @Query("total") String total,
                                   @Query("filter") String filter, @Query("type") String type,
                                   @Query("id_news_category") String id_news_category, @Query("key") String key);



    @POST("user")
    Call<RegisterResponse> postRegister(@Body RegisterRequest r);

    @POST("login")
    Call<LoginResponse> postLogin(@Body LoginRequest r);

    @POST("ganti_password")
    Call<ChangePasswordResponse> postChangePassword(@Header("token") String token,
                                                    @Body ChangePasswordRequest r);

    @POST("ganti_profil")
    Call<ChangeProfilResponse> postChangeProfil(@Header("token") String token,
                                                  @Body ChangeProfilRequest r);

    @POST("Berita_like")
    Call<LikeResponse> postLike(@Header("token") String token,
                                        @Body LikeRequest r);

    @POST("Berita_favorit")
    Call<FavoritResponse> postFavorit(@Header("token") String token,
                                   @Body FavoritRequest r);
    @POST("Berita_comment")
    Call<CommentResponse> postComment(@Header("token") String token,
                                   @Body CommentRequest r);
    @POST("update_fcm")
    Call<ChangeFCMResponse> postFCM(@Header("token") String token,
                                        @Body ChangeFCMRequest r);
    @POST("Read_open")
    Call<ReadOpenResponse> postReadOpen(@Header("token") String token,
                                   @Body ReadOpenRequest r);
    @POST("Berita_read_later")
    Call<SaveLaterResponse> postSaveLater(@Header("token") String token,
                                         @Body SaveLaterRequest r);
    @POST("Read_close")
    Call<ReadCloseResponse> postReadClose(@Header("token") String token,
                                          @Body ReadCloseRequest r);
    @GET("rangking")
    Call<RankingResponse> getRanking(@Header("token") String token, @Query("page") String page,
                                     @Query("total") String total);
}
