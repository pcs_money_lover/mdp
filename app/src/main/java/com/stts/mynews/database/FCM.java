package com.stts.mynews.database;

import io.realm.RealmObject;

public class FCM extends RealmObject {
    String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
