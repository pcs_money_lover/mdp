package com.stts.mynews.database;

import io.realm.RealmObject;

/**
 * Created by user on 2/27/18.
 */

public class User extends RealmObject {
    String username;
    String token;
    String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
