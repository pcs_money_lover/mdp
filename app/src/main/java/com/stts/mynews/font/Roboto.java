package com.stts.mynews.font;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by user on 2/2/18.
 */

public class Roboto {
    public static Typeface black(Context c){
        return Typeface.createFromAsset(c.getAssets(),  "robotoblack.ttf");
    }
    public static Typeface blackitalic(Context c){
        return Typeface.createFromAsset(c.getAssets(),  "robotoblackitalic.ttf");
    }
    public static Typeface bold(Context c){
        return Typeface.createFromAsset(c.getAssets(),  "robotobold.ttf");
    }
    public static Typeface bolditalic(Context c){
        return Typeface.createFromAsset(c.getAssets(),  "robotobolditalic.ttf");
    }
    public static Typeface italic(Context c){
        return Typeface.createFromAsset(c.getAssets(),  "robotoitalic.ttf");
    }
    public static Typeface light(Context c){
        return Typeface.createFromAsset(c.getAssets(),  "robotolight.ttf");
    }
    public static Typeface lightitalic(Context c){
        return Typeface.createFromAsset(c.getAssets(),  "robotolightItalic.ttf");
    }
    public static Typeface medium(Context c){
        return Typeface.createFromAsset(c.getAssets(),  "robotomedium.ttf");
    }
    public static Typeface mediumitalic(Context c){
        return Typeface.createFromAsset(c.getAssets(),  "robotomediumItalic.ttf");
    }
    public static Typeface regular(Context c){
        return Typeface.createFromAsset(c.getAssets(),  "robotoregular.ttf");
    }
    public static Typeface thin(Context c){
        return Typeface.createFromAsset(c.getAssets(),  "robotothin.ttf");
    }
    public static Typeface thinitalic(Context c){
        return Typeface.createFromAsset(c.getAssets(),  "robotothinItalic.ttf");
    }
}
