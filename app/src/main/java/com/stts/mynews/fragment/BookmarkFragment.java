package com.stts.mynews.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.stts.mynews.R;
import com.stts.mynews.adapter.BookmarkAdapter;
import com.stts.mynews.adapter.NewsAdapter;
import com.stts.mynews.database.BookmarkBerita;
import com.stts.mynews.helper.ServiceRealm;
import com.stts.mynews.model.Bookmark;
import com.stts.mynews.model.News;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.realm.RealmResults;

public class BookmarkFragment extends Fragment {

    public BookmarkFragment() {
        // Required empty public contructor

    }

    private BookmarkAdapter adapter;
    private List<Bookmark> list = new ArrayList<>();
    private RecyclerView recyclerView;
    private RealmResults<BookmarkBerita> bookmarkBeritas;
    private LinearLayout tidakada;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_bookmark, container, false);
        bookmarkBeritas= ServiceRealm.getrealm(getActivity().getApplicationContext()).where(BookmarkBerita.class).findAll();
        tidakada = root.findViewById(R.id.tidakada);
        if(bookmarkBeritas.size() > 0){
            tidakada.setVisibility(View.GONE);
        }
        initRecyclerView(root);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();

        initValue();
    }

    public void initValue(){
        list.clear();
            for (int i = 0; i < bookmarkBeritas.size(); i++) {

                list.add(new Bookmark("" + bookmarkBeritas.get(i).getIdBerita(),
                        "" + bookmarkBeritas.get(i).getJudul(),
                        "" + bookmarkBeritas.get(i).getDeskripsi(),
                        "" + bookmarkBeritas.get(i).getImg(),
                        "" + bookmarkBeritas.get(i).getDateTime()));
            }
        Collections.reverse(list);

        adapter.notifyDataSetChanged();
    }
    public void initRecyclerView(View root){

        recyclerView = (RecyclerView) root.findViewById(R.id.recycler_view);
        recyclerView.setFocusable(false);
        adapter = new BookmarkAdapter(getActivity(),getActivity().getApplicationContext(),list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}