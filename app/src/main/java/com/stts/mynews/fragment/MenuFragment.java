package com.stts.mynews.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.stts.mynews.R;
import com.stts.mynews.activity.DetailActivity;
import com.stts.mynews.activity.MainActivity;
import com.stts.mynews.adapter.NewsAdapter;
import com.stts.mynews.connection.ApiClientRoot;
import com.stts.mynews.connection.Rest;
import com.stts.mynews.database.User;
import com.stts.mynews.helper.DateTime;
import com.stts.mynews.helper.Pesan;
import com.stts.mynews.helper.ServiceRealm;
import com.stts.mynews.model.News;
import com.stts.mynews.response.BeritaResponse;
import com.stts.mynews.response.MyProfilResponse;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;

public class MenuFragment extends Fragment {

    public MenuFragment() {

    }

    private NewsAdapter adapter;
    private List<News> list = new ArrayList<>();
    private RecyclerView recyclerView;
    private RealmResults<User> users;
    private int page = 1;
    private TextView timefeatured,timenews;
    private TextView judulpin,deskpin;
    private ImageView imgpin;
    private CardView lpin;
    private String id="",date ="",title="",desc="",img="";
    private String video_url = "";
    private LinearLayout dummynews;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_news, container, false);
        users= ServiceRealm.getrealm(getActivity().getApplicationContext()).where(User.class).findAll();
        initComponent(root);
        initRecyclerView(root);
        if(list.size() <= 0){
            initValue();
        }
        initValuePinned();
        return root;
    }
    public void initComponent(View root){
        timefeatured = root.findViewById(R.id.timefeatured);
        timenews = root.findViewById(R.id.timenews);
        imgpin = root.findViewById(R.id.imgpin);
        judulpin = root.findViewById(R.id.judulpin);
        deskpin = root.findViewById(R.id.deskpin);
        dummynews = root.findViewById(R.id.dummynews);
        if(list.size()<=0){
            dummynews.setVisibility(View.VISIBLE);
        }else{
            dummynews.setVisibility(View.GONE);
        }
        lpin = root.findViewById(R.id.lpin);
        timenews.setText("Today, "+ DateTime.getMonth()+" 2018");
        timefeatured.setText("Today, "+ DateTime.getMonth()+" 2018");
        lpin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity().getApplicationContext(), DetailActivity.class);
                i.putExtra("id",""+id);
                i.putExtra("date",""+date);
                i.putExtra("title",""+title);
                i.putExtra("desc",""+desc);
                i.putExtra("img",""+img);
                i.putExtra("video",""+video_url);
                getActivity().startActivity(i);
            }
        });
    }
    public void initValue(){

        Rest service = new ApiClientRoot().getClient().create(Rest.class);
        Call<BeritaResponse> call = service.getBerita(""+page,"40","","desc");
        call.enqueue(new Callback<BeritaResponse>() {
            @Override
            public void onResponse(Call<BeritaResponse> call, retrofit2.Response<BeritaResponse> response) {
                if(response.body() != null){
                    if(response.body().getStatus()== 1){
                        for (int i=0;i<response.body().getData().size();i++){
                            list.add(new News(response.body().getData().get(i).getIdNews()
                                    ,response.body().getData().get(i).getTitle(),
                                    response.body().getData().get(i).getDeskripsi(),
                                    response.body().getData().get(i).getGalery(),
                                    response.body().getData().get(i).getDateTime(),
                                    response.body().getData().get(i).getVideo()));
                        }
                        adapter.notifyDataSetChanged();
                        dummynews.setVisibility(View.GONE);
                    }else{
                        Toast.makeText(getActivity().getApplicationContext(),""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<BeritaResponse> call, Throwable t) {
                Pesan.debug("coba",t.getMessage()+" ");
            }
        });
    }

    public void initValuePinned(){

        Rest service = new ApiClientRoot().getClient().create(Rest.class);
        Call<BeritaResponse> call = service.getBeritaPinned();
        call.enqueue(new Callback<BeritaResponse>() {
            @Override
            public void onResponse(Call<BeritaResponse> call, retrofit2.Response<BeritaResponse> response) {
                if(response.body() != null){
                    if(response.body().getStatus()== 1){
                        for (int i=0;i<response.body().getData().size();i++){
                            Picasso.get().load(response.body().getData().get(i).getGalery()).into(imgpin);
                            judulpin.setText(response.body().getData().get(i).getTitle());
                            deskpin.setText(response.body().getData().get(i).getDeskripsi());
                            deskpin.setTextColor(Color.argb(255,153,153,153));
                            judulpin.setTextColor(Color.argb(255,51,51,51));
                            judulpin.setBackgroundColor(Color.TRANSPARENT);
                            deskpin.setBackgroundColor(Color.TRANSPARENT);
                            id= response.body().getData().get(i).getIdNews();
                            date= response.body().getData().get(i).getDateTime();
                            title= response.body().getData().get(i).getTitle();
                            desc= response.body().getData().get(i).getDeskripsi();
                            img= response.body().getData().get(i).getGalery();
                            video_url = response.body().getData().get(i).getVideo();
                        }
                    }else{
                        Toast.makeText(getActivity().getApplicationContext(),""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<BeritaResponse> call, Throwable t) {
                Pesan.debug("coba",t.getMessage()+" ");
            }
        });
    }
    public void initRecyclerView(View root){

        recyclerView = (RecyclerView) root.findViewById(R.id.recycler_view);
        recyclerView.setFocusable(false);
        adapter = new NewsAdapter(getActivity(),getActivity().getApplicationContext(),list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }
        });
        adapter.notifyDataSetChanged();
    }
}