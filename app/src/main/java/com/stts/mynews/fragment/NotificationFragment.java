package com.stts.mynews.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.stts.mynews.R;
import com.stts.mynews.adapter.NewsAdapter;
import com.stts.mynews.adapter.NotificationAdapter;
import com.stts.mynews.database.BookmarkBerita;
import com.stts.mynews.database.NotifBerita;
import com.stts.mynews.helper.ServiceRealm;
import com.stts.mynews.model.Bookmark;
import com.stts.mynews.model.News;
import com.stts.mynews.model.Notification;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmResults;

public class NotificationFragment extends Fragment {

    private RealmResults<NotifBerita> bookmarkBeritas;
    public NotificationFragment() {
        // Required empty public contructor

    }

    private NotificationAdapter adapter;
    private List<Notification> list = new ArrayList<>();
    private RecyclerView recyclerView;
    private LinearLayout tidakada;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_notification, container, false);
        bookmarkBeritas= ServiceRealm.getrealm(getActivity().getApplicationContext()).where(NotifBerita.class).findAll();
        tidakada = root.findViewById(R.id.tidakada);
        if(bookmarkBeritas.size() > 0){
            tidakada.setVisibility(View.GONE);
        }
        initRecyclerView(root);
        initValue();
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    public void initValue(){
        if(list.size()<=0) {
            for (int i = 0; i < bookmarkBeritas.size(); i++) {

                list.add(new Notification("" + bookmarkBeritas.get(i).getIdBerita(),
                        "" + bookmarkBeritas.get(i).getJudul(),
                        "" + bookmarkBeritas.get(i).getDeskripsi(),
                        "" + bookmarkBeritas.get(i).getImg(),
                        "" + bookmarkBeritas.get(i).getDateTime()));
            }
            adapter.notifyDataSetChanged();
        }
    }
    public void initRecyclerView(View root){

        recyclerView = (RecyclerView) root.findViewById(R.id.recycler_view);
        recyclerView.setFocusable(false);
        adapter = new NotificationAdapter(getActivity(),getActivity().getApplicationContext(),list);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}