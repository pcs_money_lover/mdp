package com.stts.mynews.helper;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DateTime {
    public static String getMonth(){
        Calendar cal = Calendar.getInstance();
        String as = new SimpleDateFormat("MMM").format(cal.getTime());
        return as;
    }
    public static String getMinute(int second){
        int minute = second / 60;
        int tsecond = second % 60;
        String val = minute+" Menit "+tsecond+" Detik";
        return val;
    }
}
