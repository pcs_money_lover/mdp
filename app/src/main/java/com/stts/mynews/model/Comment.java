package com.stts.mynews.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Comment {
    private String idNewsComment;
    private String idNews;
    private String idUser;
    private String deskripsi;
    private String dateTime;

    public Comment(String idNewsComment, String idNews, String idUser, String deskripsi, String dateTime) {
        this.idNewsComment = idNewsComment;
        this.idNews = idNews;
        this.idUser = idUser;
        this.deskripsi = deskripsi;
        this.dateTime = dateTime;
    }

    public String getIdNewsComment() {
        return idNewsComment;
    }

    public void setIdNewsComment(String idNewsComment) {
        this.idNewsComment = idNewsComment;
    }

    public String getIdNews() {
        return idNews;
    }

    public void setIdNews(String idNews) {
        this.idNews = idNews;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }
}
