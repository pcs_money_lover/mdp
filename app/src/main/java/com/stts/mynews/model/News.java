package com.stts.mynews.model;

/**
 * Created by user on 3/11/18.
 */

public class News {
    private String idBerita;
    private String judul;
    private String deskripsi;
    private String img;
    private String dateTime;
    private String video;

    public News(String idBerita, String judul, String deskripsi, String img, String dateTime, String video) {
        this.idBerita = idBerita;
        this.judul = judul;
        this.deskripsi = deskripsi;
        this.img = img;
        this.dateTime = dateTime;
        this.video = video;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getIdBerita() {
        return idBerita;
    }

    public void setIdBerita(String idBerita) {
        this.idBerita = idBerita;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }
}
