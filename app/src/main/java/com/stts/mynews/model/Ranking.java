package com.stts.mynews.model;

public class Ranking {
     String name;
     String jumlah_news;
     String lama;

    public Ranking(String name, String jumlah_news, String lama) {
        this.name = name;
        this.jumlah_news = jumlah_news;
        this.lama = lama;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJumlah_news() {
        return jumlah_news;
    }

    public void setJumlah_news(String jumlah_news) {
        this.jumlah_news = jumlah_news;
    }

    public String getLama() {
        return lama;
    }

    public void setLama(String lama) {
        this.lama = lama;
    }
}
