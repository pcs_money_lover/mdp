package com.stts.mynews.request;

public class ChangePasswordRequest  {
    String old_pass;
    String new_pass;
    String confirm_pass;

    public ChangePasswordRequest(String old_pass, String new_pass, String confirm_pass) {
        this.old_pass = old_pass;
        this.new_pass = new_pass;
        this.confirm_pass = confirm_pass;
    }
}
