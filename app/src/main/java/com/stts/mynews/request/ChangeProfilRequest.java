package com.stts.mynews.request;

public class ChangeProfilRequest {
    String name;
    String email;
    String phone;

    public ChangeProfilRequest(String name, String email, String phone) {
        this.name = name;
        this.email = email;
        this.phone = phone;
    }
}
