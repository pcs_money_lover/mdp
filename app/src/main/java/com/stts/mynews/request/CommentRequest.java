package com.stts.mynews.request;

public class CommentRequest {
    String id_news;
    String deskripsi;

    public CommentRequest(String id_news, String deskripsi) {
        this.id_news = id_news;
        this.deskripsi = deskripsi;
    }
}
