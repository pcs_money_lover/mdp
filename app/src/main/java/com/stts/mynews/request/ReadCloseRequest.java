package com.stts.mynews.request;

public class ReadCloseRequest {
    String id_oldest_reader;

    public ReadCloseRequest(String id_oldest_reader) {
        this.id_oldest_reader = id_oldest_reader;
    }
}
