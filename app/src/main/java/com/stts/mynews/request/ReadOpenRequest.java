package com.stts.mynews.request;

import com.stts.mynews.response.BaseResponse;

public class ReadOpenRequest {
    String id_news;

    public ReadOpenRequest(String id_news) {
        this.id_news = id_news;
    }
}
