package com.stts.mynews.request;

public class RegisterRequest {
    String username;
    String password;
    String name;
    String email;
    String phone;

    public RegisterRequest(String username, String password, String name, String email, String phone) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.email = email;
        this.phone = phone;
    }
}
