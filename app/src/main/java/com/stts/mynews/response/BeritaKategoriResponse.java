package com.stts.mynews.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BeritaKategoriResponse extends BaseResponse {

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("id_news_category")
        @Expose
        private String idNewsCategory;
        @SerializedName("name")
        @Expose
        private String name;

        public String getIdNewsCategory() {
            return idNewsCategory;
        }

        public void setIdNewsCategory(String idNewsCategory) {
            this.idNewsCategory = idNewsCategory;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

}
