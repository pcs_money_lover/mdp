package com.stts.mynews.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BeritaResponse extends BaseResponse {

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("id_news")
        @Expose
        private String idNews;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("date_time")
        @Expose
        private String dateTime;
        @SerializedName("deskripsi")
        @Expose
        private String deskripsi;
        @SerializedName("galery")
        @Expose
        private String galery;
        @SerializedName("tipe_galery")
        @Expose
        private String tipeGalery;
        @SerializedName("id_news_category")
        @Expose
        private String idNewsCategory;
        @SerializedName("video")
        @Expose
        private String video;

        public String getVideo() {
            return video;
        }

        public void setVideo(String video) {
            this.video = video;
        }

        public String getIdNews() {
            return idNews;
        }

        public void setIdNews(String idNews) {
            this.idNews = idNews;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDateTime() {
            return dateTime;
        }

        public void setDateTime(String dateTime) {
            this.dateTime = dateTime;
        }

        public String getDeskripsi() {
            return deskripsi;
        }

        public void setDeskripsi(String deskripsi) {
            this.deskripsi = deskripsi;
        }

        public String getGalery() {
            return galery;
        }

        public void setGalery(String galery) {
            this.galery = galery;
        }

        public String getTipeGalery() {
            return tipeGalery;
        }

        public void setTipeGalery(String tipeGalery) {
            this.tipeGalery = tipeGalery;
        }

        public String getIdNewsCategory() {
            return idNewsCategory;
        }

        public void setIdNewsCategory(String idNewsCategory) {
            this.idNewsCategory = idNewsCategory;
        }

    }

}
