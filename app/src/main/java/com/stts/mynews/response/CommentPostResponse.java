package com.stts.mynews.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CommentPostResponse extends BaseResponse {

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("id_news_comment")
        @Expose
        private String idNewsComment;
        @SerializedName("id_news")
        @Expose
        private String idNews;
        @SerializedName("id_user")
        @Expose
        private String idUser;
        @SerializedName("deskripsi")
        @Expose
        private String deskripsi;
        @SerializedName("date_time")
        @Expose
        private String dateTime;
        @SerializedName("name")
        @Expose
        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIdNewsComment() {
            return idNewsComment;
        }

        public void setIdNewsComment(String idNewsComment) {
            this.idNewsComment = idNewsComment;
        }

        public String getIdNews() {
            return idNews;
        }

        public void setIdNews(String idNews) {
            this.idNews = idNews;
        }

        public String getIdUser() {
            return idUser;
        }

        public void setIdUser(String idUser) {
            this.idUser = idUser;
        }

        public String getDeskripsi() {
            return deskripsi;
        }

        public void setDeskripsi(String deskripsi) {
            this.deskripsi = deskripsi;
        }

        public String getDateTime() {
            return dateTime;
        }

        public void setDateTime(String dateTime) {
            this.dateTime = dateTime;
        }

    }


}
