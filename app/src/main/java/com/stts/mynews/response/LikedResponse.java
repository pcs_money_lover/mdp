package com.stts.mynews.response;

public class LikedResponse extends BaseResponse {
    String liked;

    public String getLiked() {
        return liked;
    }

    public void setLiked(String liked) {
        this.liked = liked;
    }
}
