package com.stts.mynews.response;

public class LoginResponse extends BaseResponse {
    String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
