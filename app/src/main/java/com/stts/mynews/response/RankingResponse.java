package com.stts.mynews.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RankingResponse extends BaseResponse {
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("lama")
        @Expose
        private String lama;
        @SerializedName("jumlah_news")
        @Expose
        private String jumlah_news;

        public String getJumlah_news() {
            return jumlah_news;
        }

        public void setJumlah_news(String jumlah_news) {
            this.jumlah_news = jumlah_news;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getLama() {
            return lama;
        }

        public void setLama(String lama) {
            this.lama = lama;
        }

    }
}
