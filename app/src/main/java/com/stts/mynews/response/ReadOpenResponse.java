package com.stts.mynews.response;

public class ReadOpenResponse extends BaseResponse {
    String id_oldest_reader;

    public String getId_oldest_reader() {
        return id_oldest_reader;
    }

    public void setId_oldest_reader(String id_oldest_reader) {
        this.id_oldest_reader = id_oldest_reader;
    }
}
