package com.stts.mynews.services;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.stts.mynews.connection.ApiClientRoot;
import com.stts.mynews.connection.Rest;
import com.stts.mynews.database.FCM;
import com.stts.mynews.database.User;
import com.stts.mynews.helper.Pesan;
import com.stts.mynews.helper.ServiceRealm;
import com.stts.mynews.model.News;
import com.stts.mynews.request.ChangeFCMRequest;
import com.stts.mynews.response.BeritaResponse;
import com.stts.mynews.response.ChangeFCMResponse;

import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private String TAG = "firebasenews";
    private RealmResults<User> users;
    private RealmResults<FCM> fcms;
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        sendRegistrationToServer(refreshedToken);
    }

    public void sendRegistrationToServer(String token){

        users= ServiceRealm.getrealm(getApplicationContext()).where(User.class).findAll();
        fcms= ServiceRealm.getrealm(getApplicationContext()).where(FCM.class).findAll();
        if(users.size() > 0) {
            new ServiceRealm().getrealm(getApplicationContext()).beginTransaction();
            fcms.get(0).setToken(token);
            new ServiceRealm().getrealm(getApplicationContext()).commitTransaction();
            Rest service = new ApiClientRoot().getClient().create(Rest.class);
            Call<ChangeFCMResponse> call = service.postFCM(users.get(0).getToken(), new ChangeFCMRequest(token));
            call.enqueue(new Callback<ChangeFCMResponse>() {
                @Override
                public void onResponse(Call<ChangeFCMResponse> call, retrofit2.Response<ChangeFCMResponse> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() == 1) {
                            Log.d("berhasil", "berhasil");
                        }
                    }
                }

                @Override
                public void onFailure(Call<ChangeFCMResponse> call, Throwable t) {
                    Pesan.debug("coba", t.getMessage() + " ");
                }
            });
        }else{
            new ServiceRealm().getrealm(getApplicationContext()).beginTransaction();
            FCM db = new ServiceRealm().getrealm(getApplicationContext()).createObject(FCM.class);
            db.setToken(token);
            new ServiceRealm().getrealm(getApplicationContext()).commitTransaction();
        }
    }
}
