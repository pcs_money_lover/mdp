package com.stts.mynews.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.stts.mynews.R;
import com.stts.mynews.activity.DetailActivity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.stts.mynews.database.BookmarkBerita;
import com.stts.mynews.database.NotifBerita;
import com.stts.mynews.helper.ServiceRealm;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

public class MyFirebaseMessagingService extends FirebaseMessagingService{
    private static final String NOTIFICATION_ID_EXTRA = "notificationId";
    private static final String IMAGE_URL_EXTRA = "imageUrl";
    private static final String ADMIN_CHANNEL_ID ="admin_channel";
    private NotificationManager notificationManager;
    private static String id_berita = "";
    private static String galery_berita = "";
    private static String deskripsi_berita = "";
    private static String video_berita = "";
    private static String title_berita = "";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        new com.stts.mynews.helper.ServiceRealm().getrealm(getApplicationContext()).beginTransaction();
        NotifBerita db = new com.stts.mynews.helper.ServiceRealm().getrealm(getApplicationContext()).createObject(NotifBerita.class);
        db.setDateTime("");
        db.setIdBerita(""+remoteMessage.getData().get("bundle.news.id_news"));
        db.setDeskripsi(""+remoteMessage.getData().get("bundle.news.deskripsi"));
        db.setImg(""+remoteMessage.getData().get("bundle.news.galery"));
        db.setJudul(""+remoteMessage.getNotification().getBody());
        db.setVideo(""+ remoteMessage.getData().get("bundle.news.video"));
        new ServiceRealm().getrealm(getApplicationContext()).commitTransaction();
        Log.d("firebasenews",""+remoteMessage.getData().get("bundle.news.id_news"));
        Log.d("firebasenews",""+remoteMessage.getData().get("bundle.news.galery"));
        Log.d("firebasenews",""+remoteMessage.getData().get("bundle.news.deskripsi"));
        id_berita = remoteMessage.getData().get("bundle.news.id_news");
        title_berita = remoteMessage.getNotification().getBody();
        galery_berita = remoteMessage.getData().get("bundle.news.galery");
        deskripsi_berita = remoteMessage.getData().get("bundle.news.deskripsi");
        video_berita = remoteMessage.getData().get("bundle.news.video");
        Intent notificationIntent = new Intent(this, DetailActivity.class);
        notificationIntent.putExtra("id",""+id_berita);
        notificationIntent.putExtra("title",""+title_berita);
        notificationIntent.putExtra("desc",""+deskripsi_berita);
        notificationIntent.putExtra("img",""+galery_berita);
        notificationIntent.putExtra("video",""+video_berita);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        final PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0 /* Request code */, notificationIntent,
                PendingIntent.FLAG_ONE_SHOT);

        //You should use an actual ID instead
        int notificationId = new Random().nextInt(60000);


        Bitmap bitmap = getBitmapfromUrl(remoteMessage.getData().get("bundle.news.galery"));

        Intent likeIntent = new Intent(this,LikeServices.class);
        likeIntent.putExtra(NOTIFICATION_ID_EXTRA,notificationId);
        likeIntent.putExtra("id",""+id_berita);
        likeIntent.putExtra("title",""+title_berita);
        likeIntent.putExtra("desc",""+deskripsi_berita);
        likeIntent.putExtra("img",""+galery_berita);
        likeIntent.putExtra("video",""+video_berita);
        PendingIntent likePendingIntent = PendingIntent.getService(this,
                notificationId+1,likeIntent,PendingIntent.FLAG_ONE_SHOT);


        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            setupChannels();
        }

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, ADMIN_CHANNEL_ID)
                        .setLargeIcon(bitmap)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(remoteMessage.getNotification().getTitle())
                        .setStyle(new NotificationCompat.BigPictureStyle()
                                .setSummaryText(remoteMessage.getNotification().getBody())
                                .bigPicture(bitmap))/*Notification with Image*/
                        .setContentText(remoteMessage.getNotification().getBody())
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .addAction(R.mipmap.ic_launcher,"Baca Sekarang",likePendingIntent)
                        .setContentIntent(pendingIntent);

        notificationManager.notify(notificationId, notificationBuilder.build());


    }

    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setupChannels(){
        CharSequence adminChannelName = getString(R.string.notifications_admin_channel_name);
        String adminChannelDescription = getString(R.string.notifications_admin_channel_description);

        NotificationChannel adminChannel;
        adminChannel = new NotificationChannel(ADMIN_CHANNEL_ID, adminChannelName, NotificationManager.IMPORTANCE_LOW);
        adminChannel.setDescription(adminChannelDescription);
        adminChannel.enableLights(true);
        adminChannel.setLightColor(Color.RED);
        adminChannel.enableVibration(true);
        if (notificationManager != null) {
            notificationManager.createNotificationChannel(adminChannel);
        }
    }
}
